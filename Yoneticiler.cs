﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5557Otomasyon.Models.Context
{
    class Yoneticiler
    {
        [Key]
        public int    ID            { get; set; }
        public string KullaniciAdi  { get; set; }
        public string Soyadı        { get; set; }
        public string Adı           { get; set; }
        public string Şifre         { get; set; }
        public string Email         { get; set; }
        public string Pozisyon      { get; set; }
        public string YoneticiKodu  { get; set; }

    }
}
