﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _5557Otomasyon.Models;

namespace _5557Otomasyon
{
    public class HomeController : Controller
    {
        OtomasyonContext context = new OtomasyonContext();

        // GET: Home
        public ActionResult Index()
        {
            var model = new ViewModel();
            model.haberler = context.HaberlerTablosu;
            model.urunler  = context.UrunlerTablosu;
            return View(model);
        }

        public ActionResult Sirketimiz()
        {
            return View();
        }

        public ActionResult Urunlerimiz()
        {
            return View(context.UrunlerTablosu);
        }
        public ActionResult Musterilerimiz()
        {
            return View(context.MusterilerTablosu);
        }
        public ActionResult Iletisim()
        {
            return View();
        }
    }
}