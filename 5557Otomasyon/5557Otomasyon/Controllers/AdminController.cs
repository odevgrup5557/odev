﻿using _5557Otomasyon.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _5557Otomasyon
{
    public class AdminController : Controller
    {
        OtomasyonContext context = new OtomasyonContext();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HaberListele()
        {
            return View(context.HaberlerTablosu);
        }

        public ActionResult HaberDuzenle(string ID)
        {
            var haber = context.HaberlerTablosu.FirstOrDefault(item => item.ID.ToString() == ID);
            if (haber == null)
                return RedirectToAction("HaberListele", "Admin");
            return View(haber);
        }
        [HttpPost]
        public ActionResult HaberDuzenle(Haberler haber)
        {
            var eskiHaber = context.HaberlerTablosu.FirstOrDefault(item => item.ID == haber.ID);
            eskiHaber.Baslik = haber.Baslik;
            eskiHaber.Icerik = haber.Icerik;
            context.SaveChanges();
            return RedirectToAction("HaberListele", "Admin");
        }

        public ActionResult HaberEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult HaberEkle(Haberler haber)
        {
            context.HaberlerTablosu.Add(haber);
            context.SaveChanges();
            return RedirectToAction("HaberListele", "Admin");
        }

        public ActionResult HaberSil(string ID)
        {
            var haber = context.HaberlerTablosu.FirstOrDefault(item => item.ID.ToString() == ID);
            context.HaberlerTablosu.Remove(haber);
            context.SaveChanges();
            return RedirectToAction("HaberListele", "Admin");
        }

        public ActionResult MusteriListele()
        {
            return View(context.MusterilerTablosu);
        }

        public ActionResult MusteriDuzenle(string ID)
        {
            var musteri = context.MusterilerTablosu.FirstOrDefault(item => item.ID.ToString() == ID);
            if (musteri == null)
                return RedirectToAction("MusteriListele", "Admin");
            return View(musteri);
        }
        [HttpPost]
        public ActionResult MusteriDuzenle(Musteriler musteri, HttpPostedFileBase file)
        {
            var eskiMusteri         = context.MusterilerTablosu.FirstOrDefault(item => item.ID == musteri.ID);
            eskiMusteri.Adi         = musteri.Adi;
            eskiMusteri.Aciklama    = musteri.Aciklama;
            eskiMusteri.Mail        = musteri.Mail;
            eskiMusteri.Telefon     = musteri.Telefon;
            eskiMusteri.WebSitesi   = musteri.WebSitesi;
            if (!string.IsNullOrEmpty(ResimKaydet(file)))
                eskiMusteri.ResimAdresi = ResimKaydet(file);
            context.SaveChanges();
            return RedirectToAction("MusteriListele", "Admin");
        }

        public ActionResult MusteriEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult MusteriEkle(Musteriler musteri, HttpPostedFileBase file)
        {
            if (!string.IsNullOrEmpty(ResimKaydet(file)))
                musteri.ResimAdresi = ResimKaydet(file);
            context.MusterilerTablosu.Add(musteri);
            context.SaveChanges();
            return RedirectToAction("MusteriListele", "Admin");
        }

        public ActionResult MusteriSil(string ID)
        {
            var musteri = context.MusterilerTablosu.FirstOrDefault(item => item.ID.ToString() == ID);
            context.MusterilerTablosu.Remove(musteri);
            context.SaveChanges();
            return RedirectToAction("MusteriListele", "Admin");
        }

        public ActionResult UrunleriListele()
        {
            return View(context.UrunlerTablosu);
        }

        public ActionResult UrunDuzenle(string ID)
        {
            var urun = context.UrunlerTablosu.FirstOrDefault(item => item.ID.ToString() == ID);
            if (urun == null)
                return RedirectToAction("UrunleriListele", "Admin");
            return View(urun);
        }

        [HttpPost]
        public ActionResult UrunDuzenle(Urunler urun, HttpPostedFileBase file)
        {
            var eskiUrun = context.UrunlerTablosu.FirstOrDefault(item => item.ID == urun.ID);
            eskiUrun.Aciklama = urun.Aciklama;
            eskiUrun.Adi      = urun.Adi;
            eskiUrun.Kodu     = urun.Kodu;
            if (!string.IsNullOrEmpty(ResimKaydet(file)))
                eskiUrun.ResimAdresi = ResimKaydet(file);
            context.SaveChanges();
            return RedirectToAction("UrunleriListele", "Admin");
        }

        public ActionResult UrunEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UrunEkle(Urunler urun, HttpPostedFileBase file)
        {
            if (!string.IsNullOrEmpty(ResimKaydet(file)))
                urun.ResimAdresi = ResimKaydet(file);
            context.UrunlerTablosu.Add(urun);
            context.SaveChanges();
            return RedirectToAction("UrunleriListele", "Admin");
        }

        public ActionResult UrunSil(string ID)
        {
            var urun = context.UrunlerTablosu.FirstOrDefault(item => item.ID.ToString() == ID);
            context.UrunlerTablosu.Remove(urun);
            context.SaveChanges();
            return RedirectToAction("UrunListele", "Admin");
        }

        public ActionResult YoneticileriListele()
        {
            return View(context.YoneticilerTablosu);
        }

        public ActionResult YoneticiDuzenle(string ID)
        {
            var yonetici = context.YoneticilerTablosu.FirstOrDefault(item => item.ID.ToString() == ID);
            if (yonetici == null)
                return RedirectToAction("YoneticileriListele", "Admin");
            return View(yonetici);
        }

        [HttpPost]
        public ActionResult YoneticiDuzenle(Yoneticiler yonetici)
        {
            var eskiYonetici            = context.YoneticilerTablosu.FirstOrDefault(item => item.ID == yonetici.ID);
            eskiYonetici.Pozisyon       = yonetici.Pozisyon;
            eskiYonetici.Sifre          = yonetici.Sifre;
            eskiYonetici.Adi            = yonetici.Adi;
            eskiYonetici.Soyadi         = yonetici.Soyadi;
            eskiYonetici.Email          = yonetici.Email;
            eskiYonetici.YoneticiKodu   = yonetici.YoneticiKodu;
            context.SaveChanges();
            return RedirectToAction("YoneticileriListele", "Admin");
        }

        public ActionResult YoneticiEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult YoneticiEkle(Yoneticiler yonetici)
        {
            var yon = context.YoneticilerTablosu.FirstOrDefault(item => item.KullaniciAdi == yonetici.KullaniciAdi);
            if (yon != null)
            {
                ViewBag.Message = "Bu kullanıcı adına sahip bir yonetici zaten mevcut";
                return View();
            }
            context.YoneticilerTablosu.Add(yonetici);
            context.SaveChanges();
            return RedirectToAction("YoneticileriListele", "Admin");    
        }

        public ActionResult YoneticiSil(string ID)
        {
            var urun = context.YoneticilerTablosu.FirstOrDefault(item => item.ID.ToString() == ID);
            context.YoneticilerTablosu.Remove(urun);
            context.SaveChanges();
            return RedirectToAction("YoneticileriListele", "Admin");
        }

        public string ResimKaydet(HttpPostedFileBase file)
        {
            string dosyaYolu = string.Empty;
            if (file != null)
            {
                dosyaYolu = Path.GetFileName(file.FileName);
                var kayitYolu = Path.Combine(Server.MapPath("~/Content/img"), dosyaYolu);
                file.SaveAs(kayitYolu);
            }
            return dosyaYolu;
        }
    }
}