﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5557Otomasyon.Models
{
    public class Yoneticiler
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int    ID            { get; set; }
        public string KullaniciAdi  { get; set; }
        public string Soyadi        { get; set; }
        public string Adi           { get; set; }
        public string Sifre         { get; set; }
        public string Email         { get; set; }
        public string Pozisyon      { get; set; }
        public string YoneticiKodu  { get; set; }

    }
}
