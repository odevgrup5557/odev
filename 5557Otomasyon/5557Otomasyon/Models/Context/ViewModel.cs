﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5557Otomasyon.Models
{
    public class ViewModel
    {
        public IEnumerable<Haberler> haberler { get; set; }
        public IEnumerable<Urunler> urunler { get; set; }
    }
}
