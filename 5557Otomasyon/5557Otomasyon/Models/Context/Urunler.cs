﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5557Otomasyon.Models
{
    public class Urunler
    {
        [Key][DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int    ID          { get; set; }
        public string Adi         { get; set; }
        public string Kodu        { get; set; }
        public string ResimAdresi { get; set; }
        public string Aciklama       { get; set; }
    }
}
