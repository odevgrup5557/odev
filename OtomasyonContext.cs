﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5557Otomasyon.Models.Context
{
    class OtomasyonContext : DbContext
    {
        public DbSet<Yoneticiler> YoneticilerTablosu { get; set; }
        public DbSet<Haberler>    HaberlerTablosu    { get; set; }
        public DbSet<Urunler>     UrunlerTablosu     { get; set; }
        public DbSet<Musteriler>  MusterilerTablosu  { get; set; }

    }
}
