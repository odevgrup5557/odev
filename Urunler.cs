﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5557Otomasyon.Models.Context
{
    class Urunler
    {
        [Key]
        public string ID        { get; set; }
        public string Adi       { get; set; }
        public string Kodu      { get; set; }
        public string ResimLink { get; set; }
        public string Bilgi     { get; set; }
    }
}
