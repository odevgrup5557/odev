﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5557Otomasyon.Models.Context
{
    class Musteriler
    {
        [Key]
        public string ID         { get; set; }
        public string Adi        { get; set; }
        public string Telefon    { get; set; }
        public string WebSitesi  { get; set; }
        public string Mail       { get; set; }
        public string ResimLinki { get; set; }
    }
}
