﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5557Otomasyon.Models.Context
{
    class Haberler
    {
        [Key]
        public string ID     { get; set; }
        public string Baslik { get; set; }
        public string Icerik { get; set; }
    }
}
